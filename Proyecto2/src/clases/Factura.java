/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 57312
 */
public class Factura {
    private String numero;
    private String descripcion;
    private List<DetalleFactura> detalle;

    public Factura(String numero, String descripcion) {
        this.numero = numero;
        this.descripcion = descripcion;
        this.detalle=new ArrayList();
    }

    
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<DetalleFactura> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<DetalleFactura> detalle) {
        this.detalle = detalle;
    }
    
    public void adicionarItem(DetalleFactura item){
        this.detalle.add(item);
    }
    
    public Double getTotal(){
        Double total=0.0;
        for (int i = 0; i < detalle.size(); i++) {
            DetalleFactura item = detalle.get(i);
            total+=item.getSubTotal();
        }
        return total;
    }
}
