/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package proyecto2;

import clases.DetalleFactura;
import clases.Factura;
import java.util.Scanner;

/**
 *
 * @author 57312
 */
public class FacturaTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner escaner=new Scanner(System.in);
               
        System.out.println("ELABORACIÓN DE UNA FACTURA");
        System.out.print("\nNúmero: ");
        String numero=escaner.nextLine();
        System.out.print("Descripción: ");
        String descripcion=escaner.nextLine();
        
        Factura factura=new Factura(numero,descripcion);
        
        String opcion="S";
        int i=0;
        do{
            i++;
            System.out.println("Adicionando el producto " + i +" a la factura");
            System.out.print("Nombre del producto: ");
            String nombre=escaner.nextLine();
            System.out.print("Precio unitario: ");
            Double costo=escaner.nextDouble();
            System.out.print("Cantidad: ");
            Integer cantidad=escaner.nextInt();
            
            DetalleFactura detalle=new DetalleFactura(nombre, cantidad, costo);
            System.out.println("Total producto: " + detalle.getSubTotal());
            factura.adicionarItem(detalle);
            System.out.println("Total acumulado en factura: " + factura.getTotal());
            
            System.out.print("\n Desea adicionar otro artículo? [S/n]: ");
            opcion=escaner.nextLine();opcion=escaner.nextLine();
        } while(!opcion.equalsIgnoreCase("N"));
        
        System.out.println("\n FACTURA");
        System.out.println("Código: ");
        System.out.println("Descripción: ");
        System.out.println("Item\tProducto\tPrecio\tCantidad\tSubtotal");
        for (i = 0; i < factura.getDetalle().size(); i++) {
            DetalleFactura item = factura.getDetalle().get(i);
            System.out.println(i+1 + "\t" + item.getArticulo() +"\t"+ item.getPrecio()+"\t"+item.getCantidad()+"\t"+item.getSubTotal());
        }
        System.out.println("Total de la factura: " + factura.getTotal());
    }
    
}
