/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package proyecto3;

import clases.Empleado;

/**
 *
 * @author 57312
 */
public class EmpleadoTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Empleado empleado1=new Empleado("Pedro Antonio", "Mora Sanchez", 100.0);
        System.out.println("Empleado 1: " + empleado1);
        Empleado empleado2=new Empleado("Camila Fernanda", "Quiñonez Gutierrez", 150.0);        
        System.out.println("Empleado 2: " + empleado2);
        System.out.println("\nIncrementando el 10% al salario de cada empleado");
        empleado1.incrementarSalario(10);
        empleado2.incrementarSalario(10);
        System.out.println("\nEmpleado 1: " + empleado1);
        System.out.println("Salario anual: " + empleado1.getSalarioAnual());
        System.out.println("\nEmpleado 2: " + empleado2);
        System.out.println("Salario anual: " + empleado2.getSalarioAnual());
    }
    
}
