/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author 57312
 */
public class Empleado {
    private String nombre;
    private String apellido;
    private Double salario;
    
    public static Integer cantidadEmpleados=0;

    public Empleado(String nombre, String apellido, Double salario) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.salario = salario;
        cantidadEmpleados++;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    @Override
    public String toString() {
        return "Empleado{" + "nombre=" + nombre + ", apellido=" + apellido + ", salario=" + salario + '}' + "Cantidad de empleados a la fecha: " + cantidadEmpleados;
    }
    
    public void incrementarSalario(Integer incremento){//incremento en porcentaje
        this.salario+=this.salario*incremento/100;
    }
    
    public Double getSalarioAnual(){
        return salario*12;
    }
}
