/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package pruebageneration;

import java.util.Scanner;
import pruebageneration.clases.Auto;
import pruebageneration.clases.Direccion;
import pruebageneration.clases.Marca;
import pruebageneration.clases.Propietario;

/**
 *
 * @author 57312
 */
public class PruebaGeneration {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner escaner=new Scanner(System.in);
        
        Direccion direccion1=new Direccion("Calle 25 No. 23-12", "Barrio Las Américas", "Santiago de Cali", "760001", "Frente al parque");
        Propietario propietario1=new Propietario("Julian Alfredo Morales Perez", "98000000", "RG de Julian", direccion1);
        
        Marca marca1=new Marca("01", "Nissan", 10, 2020);
        Auto auto1= new Auto(propietario1);
        auto1.setModelo("Sentra Nismo");
        auto1.setColor("Rojo");
        auto1.setAnio(2020);
        auto1.setMarca(marca1);
        auto1.setChasis("13809jlkasjd12");
        auto1.setPropietario(propietario1);
        auto1.setVelocidadMaxima(200);
        auto1.setPuertos(6);
        auto1.setTechoSolar(true);
        auto1.setMarchas(5);
        auto1.setTransmisionAutomatica(false);
        
        System.out.println("Creado el vehículo " + auto1.getMarca().getNombre() +" modelo " + auto1.getModelo()
            + " de propiedad de " + auto1.getPropietario().getNombre() +" residente en " + auto1.getPropietario().getDireccion().getCiudad());
        System.out.println(auto1);
        System.out.println("Velocidad máxima: " + auto1.getVelocidadMaxima() + " Número de marchas: " + auto1.getMarchas());
        
        System.out.println("\n Utiliza las siguientes opciones para conducir el vehículo");
        int opcion;
        do {
            System.out.println("1. Incrementar velocidad (acelerar)");
            System.out.println("2. Detener el vehículo");
            System.out.println("3. Aumentar la marcha");
            System.out.println("4. Disminuir la marcha");
            System.out.println("0. Salir");
            System.out.print("\nSelecciona una opción: ");
            opcion=escaner.nextInt();
            switch(opcion){
                case 1: auto1.acelerar(); break;
                case 2: auto1.frenar();break;
                case 3: auto1.cambiarMarcha();break;
                case 4: auto1.reducirMarcha(); break;                
            }
            System.out.println("\n Estado del vehículo: " + auto1);
            System.out.println("Velocidad actual: "+ auto1.getVelocidadActual() + "\nMarcha actual: " + auto1.getMarchaActual());
        } while(opcion!=0);
    }
    
}
