/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pruebageneration.clases;

import java.util.Date;

/**
 *
 * @author 57312
 */
public class Propietario {
    private String nombre;
    private String cpf;//identificación (Documento de identidad financiero)
    private String rg;//qué significa este dato?
    private Date fechaNacimiento;
    private Direccion direccion;
    
    public Propietario(String nombre, String cpf, String rg, Direccion direccion) {
        this.nombre = nombre;
        this.cpf = cpf;
        this.rg = rg;
        this.direccion=direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }
    
    
}
