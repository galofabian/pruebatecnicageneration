/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pruebageneration.clases;

/**
 *
 * @author 57312
 */
public class Marca {
    private String codigo;//código identificador
    private String nombre;
    private Integer numeroModelos;
    private Integer anioLanzamiento;

    public Marca(String codigo, String nombre, Integer numeroModelos, Integer anioLanzamiento) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.numeroModelos = numeroModelos;
        this.anioLanzamiento = anioLanzamiento;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getNumeroModelos() {
        return numeroModelos;
    }

    public void setNumeroModelos(Integer numeroModelos) {
        this.numeroModelos = numeroModelos;
    }

    public Integer getAnioLanzamiento() {
        return anioLanzamiento;
    }

    public void setAnioLanzamiento(Integer anioLanzamiento) {
        this.anioLanzamiento = anioLanzamiento;
    }
    
    
}
