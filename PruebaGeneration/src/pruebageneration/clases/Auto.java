/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pruebageneration.clases;

/**
 *
 * @author 57312
 */
public class Auto {
    private String modelo;
    private String color;
    private Integer anio;
    private Marca marca;
    private String chasis;
    private Propietario propietario;
    private Integer velocidadMaxima;//en km/h
    private Integer velocidadActual;
    private Integer puertos;
    private boolean techoSolar;
    private Integer marchas;//número de marchas
    private boolean transmisionAutomatica;

    private Integer marchaActual;
    
    public Auto(Propietario propietario) {
        this.propietario = propietario;
        this.velocidadActual=0;
        this.marchaActual=0;//0 es en neutro
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public String getChasis() {
        return chasis;
    }

    public void setChasis(String chasis) {
        this.chasis = chasis;
    }

    public Propietario getPropietario() {
        return propietario;
    }

    public void setPropietario(Propietario propietario) {
        this.propietario = propietario;
    }

    public Integer getVelocidadMaxima() {
        return velocidadMaxima;
    }

    public void setVelocidadMaxima(Integer velocidadMaxima) {
        this.velocidadMaxima = velocidadMaxima;
    }

    public Integer getVelocidadActual() {
        return velocidadActual;
    }

    public void setVelocidadActual(Integer velocidadActual) {
        this.velocidadActual = velocidadActual;
    }

    public Integer getPuertos() {
        return puertos;
    }

    public void setPuertos(Integer puertos) {
        this.puertos = puertos;
    }

    public boolean isTechoSolar() {
        return techoSolar;
    }

    public void setTechoSolar(boolean techoSolar) {
        this.techoSolar = techoSolar;
    }

    public Integer getMarchas() {
        return marchas;
    }

    public void setMarchas(Integer marchas) {
        this.marchas = marchas;
    }

    public boolean isTransmisionAutomatica() {
        return transmisionAutomatica;
    }

    public void setTransmisionAutomatica(boolean transmisionAutomatica) {
        this.transmisionAutomatica = transmisionAutomatica;
    }

    public Integer getMarchaActual() {
        return marchaActual;
    }

    public void setMarchaActual(Integer marchaActual) {
        this.marchaActual = marchaActual;
    }  
    
    public void acelerar(){
        if (this.velocidadActual < this.velocidadMaxima) {
            if (this.marchaActual!=0) this.velocidadActual+=1;
            else {
                if(this.transmisionAutomatica){
                    cambiarMarcha();
                    this.velocidadActual+=1;
                } else System.out.println("Es necesario cambiar la marcha para poder acelerar");
            }
            
        }//aumenta 1 km por hora
    }
    
    public void frenar(){
        this.velocidadActual=0;
    }
    
    public void cambiarMarcha(){//investigar significado
        this.marchaActual+=1;//cambio de marcha hacia arriba
    }
    
    public void reducirMarcha(){//investigar significado
        if (this.marchaActual>0) this.marchaActual-=1;
        else {
            if (this.velocidadActual==0) this.marchaActual-=1;//-1 es reversa
        }
    }
    
    public void calcularAutonomia(Integer consumoMedio){//calcular la autonomía del vehículo con base al consumo medio pasado en el parámetro
        //dividimos la capacidad de la batería (expresada en kWh) por el consumo medio (en kWh/100 km) y el resultado lo multiplicamos por 100.
        //para implementar este método hacen falta añadir atributos a la clase
    }
    
    public void mostrarVolumenCombustible(){
        //para implementar este método hacen falta añadir atributos a la clase
    }

    @Override
    public String toString() {
        return "Auto{" + "modelo=" + modelo + ", color=" + color + ", anio=" + anio + ", marca=" + marca + ", chasis=" + chasis + ", propietario=" + propietario + ", velocidadMaxima=" + velocidadMaxima + ", velocidadActual=" + velocidadActual + ", puertos=" + puertos + ", techoSolar=" + techoSolar + ", marchas=" + marchas + ", transmisionAutomatica=" + transmisionAutomatica + ", marchaActual=" + marchaActual + '}';
    }
    
    
}
