/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pruebageneration.clases;

/**
 *
 * @author 57312
 */
public class Direccion {
    private String calle;
    private String vecindario;
    private String ciudad;
    //private String expresar; //qué significa?
    private String codigoPostal;
    private String complementao;//complemento de la dirección

    public Direccion(String calle, String vecindario, String ciudad, String codigoPostal, String complementao) {
        this.calle = calle;
        this.vecindario = vecindario;
        this.ciudad = ciudad;
        this.codigoPostal = codigoPostal;
        this.complementao = complementao;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getVecindario() {
        return vecindario;
    }

    public void setVecindario(String vecindario) {
        this.vecindario = vecindario;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getComplementao() {
        return complementao;
    }

    public void setComplementao(String complementao) {
        this.complementao = complementao;
    }

    
    
}
