/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Clases;

/**
 *
 * @author 57312 . 
 */
public class IntegerSet {
    //private Integer [] enteros;
    private boolean [] matriz;
    //private Integer cantidadEnteros=0;
    
    public IntegerSet() {
        //enteros=new Integer[100];        
        //cantidadEnteros=0;
        matriz=new boolean[101];
    }
    
    public void insertsElement(Integer i){
        /*enteros[cantidadEnteros]=i;
        cantidadEnteros++;*/
        if (i>0 && i<=100) matriz[i]=true;
        else System.out.println("El número debe estar entre 0 y 100");
    }
    
    public void deleteElement(Integer i){
        if (i>0 && i<=100) matriz[i]=false;
        else System.out.println("El número debe estar entre 0 y 100");        
    }
    
    public String toSetString(){
        String resultado="";
        for (int i = 0; i < 100; i++) {
            if (matriz[i]) resultado+=" "+i;
        }
        if (resultado=="") resultado="-";
        return resultado;
    }
    
    public boolean equalTo(IntegerSet conjunto){
        boolean resultado=true;
        int i=0;
        while(i<100 && resultado!=false){
            if (this.matriz[i]!=conjunto.matriz[i]) resultado=false;
            i++;
        }
        return resultado;
    }
    
    public static IntegerSet union(IntegerSet conjunto1, IntegerSet conjunto2){
        IntegerSet resultado=new IntegerSet();
        for (int i = 0; i < 99; i++) {
            //if (conjunto1[i] || conjunto2[i]) resultado[i]=true;
            resultado.matriz[i]=conjunto1.matriz[i] || conjunto2.matriz[i];
        }
        return resultado;
    }

    public static IntegerSet interseccion(IntegerSet conjunto1, IntegerSet conjunto2){
        IntegerSet resultado=new IntegerSet();
        for (int i = 0; i < 99; i++) {
            //if (conjunto1[i] || conjunto2[i]) resultado[i]=true;
            resultado.matriz[i]=conjunto1.matriz[i] && conjunto2.matriz[i];
        }
        return resultado;
    }
    
    
}
