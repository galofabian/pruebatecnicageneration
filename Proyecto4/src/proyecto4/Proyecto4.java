/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package proyecto4;

import Clases.IntegerSet;

/**
 *
 * @author 57312
 */
public class Proyecto4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        IntegerSet conjunto1=new IntegerSet();
        System.out.println("Conjunto1 vacío:  " + conjunto1.toSetString());
        
        IntegerSet conjunto2=new IntegerSet();
        System.out.println("Conjunto2 vacío:  " + conjunto1.toSetString());
        
        System.out.println("Conjunto1 y conjunto2 son iguales? "+ conjunto1.equalTo(conjunto2));
        
        System.out.println("Cargando datos conjunto 1");
        for (int i = 1; i < 10; i++) {
            conjunto1.insertsElement(i);
        }
        System.out.println("Conjunto1 con datos:  " + conjunto1.toSetString());

        System.out.println("Cargando datos conjunto 2");
        for (int i = 5; i < 20; i++) {
            conjunto2.insertsElement(i);
        }
        System.out.println("Conjunto2 con datos:  " + conjunto2.toSetString());
                        
        System.out.println("Conjunto1 y conjunto2 son iguales? "+ conjunto1.equalTo(conjunto2));
        
        System.out.println("Eliminando el dato 5 de conjunto 1");
        conjunto1.deleteElement(5);
        System.out.println("Conjunto1 con el elemento 5 eliminado "+conjunto1.toSetString());
        
        IntegerSet union=IntegerSet.union(conjunto1, conjunto2);
        System.out.println("Resultado de unir los 2 conjuntos anteriores:  " + union.toSetString());

        IntegerSet interseccion=IntegerSet.interseccion(conjunto1, conjunto2);
        System.out.println("Resultado de aplicar la intersección entre conjunto1 y conjunto2:  " + interseccion.toSetString());
        
        
    }
    
}
